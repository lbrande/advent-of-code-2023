import re
import utils


lines = utils.read_input("input_03.txt").splitlines()
symbols = set(
    (i, j)
    for i, line in enumerate(lines)
    for j, char in enumerate(line)
    if char != "." and not char.isdigit()
)
print(
    sum(
        int(number.group())
        for i, line in enumerate(lines)
        for number in re.finditer(r"\d+", line)
        if any(
            (i2, j) in symbols
            for i2 in range(i - 1, i + 2)
            for j in range(number.start() - 1, number.end() + 1)
        )
    )
)
