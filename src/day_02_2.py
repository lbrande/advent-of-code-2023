import math
import re
import utils


def num_cubes(string, color):
    return max(map(int, re.findall(rf"(\d+) {color}", string)))


print(
    sum(
        math.prod(num_cubes(line, color) for color in ["red", "green", "blue"])
        for line in utils.read_input("input_02.txt").splitlines()
    )
)
