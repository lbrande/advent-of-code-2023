from os import path


def read_input(filename: str) -> str:
    input_path = path.join(path.dirname(__file__), '../input', filename)
    return open(input_path).read()
