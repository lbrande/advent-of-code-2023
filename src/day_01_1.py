import utils


print(
    sum(
        int(digits[0] + digits[-1])
        for line in utils.read_input("input_01.txt").splitlines()
        for digits in [list(filter(str.isdigit, line))]
    )
)
