import re
import utils


def has_at_most(string, color, number):
    return all(int(x) <= number for x in re.findall(rf"(\d+) {color}", string))


print(
    sum(
        int(line.split()[1][:-1])
        for line in utils.read_input("input_02.txt").splitlines()
        if (
            has_at_most(line, "red", 12)
            and has_at_most(line, "green", 13)
            and has_at_most(line, "blue", 14)
        )
    )
)
