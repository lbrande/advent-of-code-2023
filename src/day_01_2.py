import utils


def digit_at(string, index):
    if string[index].isdigit():
        return string[index]
    elif string[index:].startswith("one"):
        return "1"
    elif string[index:].startswith("two"):
        return "2"
    elif string[index:].startswith("three"):
        return "3"
    elif string[index:].startswith("four"):
        return "4"
    elif string[index:].startswith("five"):
        return "5"
    elif string[index:].startswith("six"):
        return "6"
    elif string[index:].startswith("seven"):
        return "7"
    elif string[index:].startswith("eight"):
        return "8"
    elif string[index:].startswith("nine"):
        return "9"
    return ""


print(
    sum(
        int(digits[0] + digits[-1])
        for line in utils.read_input("input_01.txt").splitlines()
        for digits in ["".join(digit_at(line, i) for i in range(len(line)))]
    )
)
