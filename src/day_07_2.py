import utils


def value(card):
    match card:
        case "A":
            return 13
        case "K":
            return 12
        case "Q":
            return 11
        case "J":
            return 1
        case "T":
            return 10
        case n:
            return int(n)


def strength(hand):
    counts = {}
    for card in hand:
        counts[card] = counts.get(card, 0) + 1
    jokers = counts.pop("J", 0)
    counts = sorted(counts.values())
    if len(counts) == 0:
        counts = [0]
    counts[-1] += jokers
    match counts:
        case [5]:
            strength = 6
        case [1, 4]:
            strength = 5
        case [2, 3]:
            strength = 4
        case [1, 1, 3]:
            strength = 3
        case [1, 2, 2]:
            strength = 2
        case [1, 1, 1, 2]:
            strength = 1
        case _:
            strength = 0
    for card in hand:
        strength *= 15
        strength += value(card)
    return strength


print(
    sum(
        (i + 1) * bid
        for i, (_, bid) in enumerate(
            sorted(
                (strength(hand), int(bid))
                for line in utils.read_input("input_07.txt").splitlines()
                for hand, bid in [line.split(" ")]
            )
        )
    )
)
