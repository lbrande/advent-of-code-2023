import re
import utils


def get_dest_num(map_, num):
    for dest_range_start, src_range_start, range_len in map_:
        if num in range(src_range_start, src_range_start + range_len):
            return dest_range_start + num - src_range_start
    return num


nums, *maps = utils.read_input("input_05.txt").split("\n\n")
nums = map(int, nums.split(" ")[1:])
maps = {
    src_type: (dest_type, [list(map(int, line.split(" "))) for line in ranges])
    for map_ in maps
    for title, *ranges in [map_.splitlines()]
    for src_type, _, dest_type, *_ in [re.split(r"-| ", title)]
}
src_type = "seed"
while src_type in maps:
    nums = [get_dest_num(maps[src_type][1], src_num) for src_num in nums]
    src_type = maps[src_type][0]
print(min(nums))
