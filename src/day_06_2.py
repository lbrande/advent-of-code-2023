import utils


time, distance = [
    int("".join(filter(str.isdigit, line)))
    for line in utils.read_input("input_06.txt").splitlines()
]
min, max = 0, (time + 1) // 2
while min < max:
    mid = (min + max) // 2
    if mid * (time - mid) > distance:
        max = mid
    else:
        min = mid + 1
print(int(((time + 1) / 2 - min) * 2))
