import math
import re
import utils


lines = utils.read_input("input_03.txt").splitlines()
symbols = {
    (i, j): []
    for i, line in enumerate(lines)
    for j, char in enumerate(line)
    if char == "*"
}
for i, line in enumerate(lines):
    for num in re.finditer(r"\d+", line):
        for i2 in range(i - 1, i + 2):
            for j in range(num.start() - 1, num.end() + 1):
                if (i2, j) in symbols:
                    symbols[(i2, j)].append(int(num.group()))
print(sum(math.prod(nums) for nums in symbols.values() if len(nums) == 2))
