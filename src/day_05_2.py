import re
import utils


ranges, *maps = utils.read_input("input_05.txt").split("\n\n")
ranges = [
    (int(range_start), int(range_start) + int(range_len))
    for range_start, range_len in re.findall(r"(\d+) (\d+)", ranges)
]
maps = {
    src_type: (dest_type, [list(map(int, line.split(" "))) for line in ranges])
    for map_ in maps
    for title, *ranges in [map_.splitlines()]
    for src_type, _, dest_type, *_ in [re.split(r"-| ", title)]
}
src_type = "seed"
while src_type in maps:
    dest_ranges = []
    for range_start, range_end in ranges:
        for dest_range_start, src_range_start, range_len in maps[src_type][1]:
            src_range_end = src_range_start + range_len
            dest_diff = dest_range_start - src_range_start
            remaining_ranges = []
            if range_end <= src_range_start or range_start >= src_range_end:
                remaining_ranges.append((range_start, range_end))
            else:
                dest_ranges.append(
                    (
                        max(range_start, src_range_start) + dest_diff,
                        min(range_end, src_range_end) + dest_diff,
                    )
                )
                if range_start < src_range_start:
                    remaining_ranges.append((range_start, src_range_start))
                if range_end > src_range_end:
                    remaining_ranges.append((src_range_end, range_end))
        ranges = remaining_ranges
    ranges.extend(dest_ranges)
    src_type = maps[src_type][0]
print(min([range_start for range_start, _ in ranges]))
