import re
import utils


print(
    sum(
        1 << (len(numbers) - len(number_set) - 1)
        for line in utils.read_input("input_04.txt").splitlines()
        for numbers in [re.findall(r"(\d+) ", line + " ")]
        for number_set in [set(numbers)]
        if len(number_set) < len(numbers)
    )
)
