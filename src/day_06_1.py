import math
import re
import utils


times, distances = [
    re.findall(r"\d+", line) for line in utils.read_input("input_06.txt").splitlines()
]
print(
    math.prod(
        sum(i * (int(time) - i) > int(distance) for i in range(int(time)))
        for time, distance in zip(times, distances)
    )
)
