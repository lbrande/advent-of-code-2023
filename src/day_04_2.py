import re
import utils


cards = {}
for i, line in enumerate(utils.read_input("input_04.txt").splitlines()):
    cards[i] = cards.get(i, 0) + 1
    numbers = re.findall(r"(\d+) ", line + " ")
    number_set = set(numbers)
    if len(number_set) < len(numbers):
        for j in range(i + 1, i + 1 + len(numbers) - len(number_set)):
            cards[j] = cards.get(j, 0) + cards.get(i)
print(sum(cards.values()))
